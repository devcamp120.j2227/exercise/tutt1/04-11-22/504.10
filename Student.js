import Person from "./Person.js"

class Student extends Person {
    constructor(personName,personAge,gender,standard,collegeName,grade) {
        super(personName,personAge,gender)
        this.standard = standard
        this.collegeName = collegeName
        this.grade = grade
    }
    getStudentInfo() {
        console.log("Tiêu chuẩn: " + this.standard);
        console.log("Grade: " + this.grade);
        console.log("Trường: " + this.collegeName);
    }
    getGrade() {
        return this.grade;
    }
    setGrade(paramGrade) {
        this.grade = paramGrade;
    }
}

export default Student;
