import Person from "./Person.js"
import Student from "./Student.js"

let person1 = new Person("Hà Quang Tiến", 24, "male");
person1.getPersonInfo();

console.log(person1 instanceof Person);


let student1 = new Student("Hà Quang Tiến", 24, "male", "ĐHCQ", "Học Viện Tài Chính", 10);
student1.getPersonInfo();
student1.getStudentInfo();
student1.getGrade();
student1.setGrade();
console.log(student1 instanceof Person);