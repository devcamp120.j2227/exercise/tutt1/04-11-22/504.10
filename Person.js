class Person {
    constructor(personName,personAge,gender) {
        this.personName = personName
        this.personAge = personAge
        this.gender = gender
    }
    getPersonInfo() {
        console.log("Tên: " + this.personName);
        console.log("Tuổi: " + this.personAge);
        console.log("Giới tính: " + this.gender);
    }
}

export default Person